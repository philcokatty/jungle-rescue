#include <iostream>
#include <string>
#include <random>
#include "upc.h"

using namespace std;
using namespace System;

int randomAvion();

void selectAvion(int num, int cr);

int randint(int min, int max, random_device& rd) {
    mt19937_64 gen(rd());
    return gen() % (max - min) + min;
}

int colorAvion(int color) {
    foreground(randint(0, 16));
    return 0;
}

int randomAvion() {
    random_device rd;
    int numRandom = randint(1, 9, rd);
    int colorAvion = randint(0, 16, rd);
    selectAvion(numRandom, colorAvion);
    return numRandom;
}

void printAvion_1(int color) {
    cout << "\n";
    colorAvion(color);
    cout << "      __|__" << "\n";
    cout << "*---o--(_)--o---*" << "\n";
}
void printAvion_2(int color) {
    cout << "\n";
    colorAvion(color);
    cout << "   _|_" << "\n";
    cout << "---(*)---" << "\n";
    cout << "  \" ' \"" << "\n";

}
void printAvion_3(int color) {
    cout << "\n";
    colorAvion(color);
    cout << "      _________" << "\n";
    cout << "          |" << "\n";
    cout << "__________|__________" << "\n";
    cout << "       [/___\\]" << "\n";
    cout << "        \\_o_/" << "\n";

}
void printAvion_4(int color) {
    cout << "\n";
    colorAvion(color);
    cout << "          _____" << "\n";
    cout << "           _|_" << "\n";
    cout << "__________( * )__________" << "\n";
    cout << "       /   `-'   \\" << "\n";
    cout << "      O           O" << "\n";

}
void printAvion_5(int color) {
    cout << "\n";
    colorAvion(color);
    cout << "       __|__" << "\n";
    cout << "------oo(_)oo------" << "\n";

}
void printAvion_6(int color) {
    cout << "\n";
    colorAvion(color);
    cout << "            |" << "\n";
    cout << "        ___/\"\\___" << "\n";
    cout << "__________/ o \\__________" << "\n";
    cout << "   *   *  \\___/  *   *" << "\n";

}
void printAvion_7(int color) {
    cout << "\n";
    colorAvion(color);
    cout << "   __|__" << "\n";
    cout << "\\___(o)___/" << "\n";
    cout << "   ! ! !" << "\n";

}
void printAvion_8(int color) {
    cout << "\n";
    colorAvion(color);
    cout << "      __!__" << "\n";
    cout << "^----o-(_)-o----^" << "\n";


}


void selectAvion(int num, int cr) {
    switch (num) {
    case 1:
        printAvion_1(cr);
        break;
    case 2:
        printAvion_2(cr);
        break;
    case 3:
        printAvion_3(cr);
        break;
    case 4:
        printAvion_4(cr);
        break;
    case 5:
        printAvion_5(cr);
        break;
    case 6:
        printAvion_6(cr);
        break;
    case 7:
        printAvion_7(cr);
        break;
    case 8:
        printAvion_8(cr);
        break;
    }
}
